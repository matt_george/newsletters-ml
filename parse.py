


def parse(inFile):
    sets = []
    with open(inFile) as f:
        for line in iter(f.readline, ''):
            line = line.rstrip('\n')
            rlist = line.split(",")
            rlist[0] = (line[0] == 't')
            sets.append(rlist)
            
    return sets
