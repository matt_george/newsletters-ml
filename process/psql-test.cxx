
#include <iostream>
#include <algorithm>
#include <pqxx/pqxx>
#include <regex>

using namespace std;

vector<string> create_wl ( pqxx::connection &c ) :
  /** ppxx::connection -> vector<string> 
      Returns a vector of all unique words from the 
      selected result */
{
  string query = 
    "SELECT textbody"
    " FROM emails as e"
    " LIMIT 10;";
  
  pqxx::nontransaction n(c); 
  pqxx::result r(n.exec(query)); 
  
  vector<string> ret; 
  regex re("\\s+");
  for (auto it = r.begin(); it != r.end(); ++it) {
    string tbody = (*it)[0].c_str() ;
    sregex_iterator tstart(tbody.begin(), tbody.end(), re);
    cout << tbody.substr(0,40) << endl;
  }
  return ret;
}


int get_gridlock ( pqxx::connection &c ) 
{
  string query = 
    "SELECT newsletter_id, coder, gridlock, snippet"
    " FROM assertion as a"                                             
    " WHERE a.gridlock"                                                 
    " limit 10;"; 

  pqxx::nontransaction n(c); 
  pqxx::result r(n.exec(query)); 

  for (auto it = r.begin(); it != r.end(); ++it) {
    for (auto col = it->begin(); col != it->end(); ++col) {
      cout << col << ",";
    }
    cout << endl;
  }
  
}


int main () 
{
  const string config = 
    "dbname=harbridge_newsletters"
    " hostaddr=52.14.46.49"
    " password=pascalalperjanna"
    " user=nuitrcsuser1";

  pqxx::connection c(config);
  
  if ( !c.is_open()) return 1;

  cout << "Successfully opened " << c.dbname() << endl; 
  
  //get_gridlock(c); 
  create_wl(c); 

  c.disconnect();

  return 0; 
}
