
#include <iostream>
#include <fstream>
#include <algorithm>
#include <unordered_map>
#include <string>
#include <iterator> 

#include <pqxx/pqxx>

#include <boost/tokenizer.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/scoped_ptr.hpp>
#include <boost/algorithm/string/join.hpp>


#define uint unsigned int
#define FREQ_THRESHOLD 40

using namespace std;

bool normp (char x) 
{
  return !(isspace(x) || isalnum(x));
}


bool garbage_p (string s) 
/** Filter out http strings */
{
  return boost::algorithm::contains(s, "http");
}


void add_or_inc (unordered_map<string, uint> *m, string s)
/** Insert the pair into the given map, or inc pair
    if it already exists */
{
  auto e_ptr = m->find(s);
  if ( e_ptr != m->end() ) e_ptr->second++;
  else {
    pair<string, uint> p(s, 1);
    m->insert(p); 
  }
}

uint d_tokens = 0;

void 
add_tokens (unordered_map<string, uint>* map, string s)
{
  boost::algorithm::to_lower(s); 
  s.erase(std::remove_if(s.begin(), s.end(), normp), s.end());
  
  // Iterate through tokens, adding if not garbage 
  typedef boost::tokenizer<boost::char_separator<char>> tokenizer; 
  tokenizer tok(s);
  for (tokenizer::iterator t_it = tok.begin(); t_it != tok.end(); ++t_it) {
    if ( !garbage_p(*t_it) ) {
      d_tokens++;
      add_or_inc(map, *t_it);
    }
  }
}

unordered_map<string, uint> * 
get_wordmap (const pqxx::result &r, const uint row) 
{
  auto *ret = new unordered_map<string, uint>(); 
  for (auto it = r.begin(); it != r.end(); ++it) {
    if ( !(*it)[row].is_null()) {
      add_tokens(ret, (*it)[row].as<std::string>());
    }
  }
  return ret; 
}

unordered_map<string, uint> *
get_wordmap (const pqxx::tuple &t, const uint row)
{
  auto *ret = new unordered_map<string, uint>(); 
  if ( !t[row].is_null()) {
    add_tokens(ret, t[row].as<std::string>());
  }
  return ret;
}

/** 
    Get a hash_map of all unique words from within the 
    corpus
*/
unordered_map<string, uint> *
create_wl (pqxx::connection &c)
{
  string query = 
    "SELECT E.textbody, A.gridlock"
    " FROM emails as E, assertion as A "
    " WHERE E.id = A.newsletter_id " ;
    //    " LIMIT 10;";

  std::ofstream of("out.txt"); 

  pqxx::nontransaction n(c); 
  pqxx::result r(n.exec(query)); 
  uint len_a = r.size(); 
  std::cout << "Found " << len_a << " documents" << std::endl; 

  // First loop through the entire corpus, generating a word vector
  boost::scoped_ptr<unordered_map<string, uint>> wm_corpus{get_wordmap(r,0)};
  std::vector<string> corpus_words; // store the order of words that appear in collection
  
  #ifdef _ARIF_
  of << "@attribute gridlockp string"  << std::endl; 

  for (auto it = wm_corpus->begin(); it != wm_corpus->end(); ++it) {
    of << "@attribute " << it->first << " numeric" << std::endl; 
    corpus_words.push_back(it->first); 
  }
  
  of << std::endl << "@data" << std::endl; 
  #else

  for (auto it = wm_corpus->begin(); it != wm_corpus->end(); ++it) 
    corpus_words.push_back(it->first); 

  #endif

  // Loop through each document & generate representation
  uint x_a = 1;
  for (auto it = r.begin(); it != r.end(); ++it, ++x_a) {

    if (!(clock() % CLOCKS_PER_SEC)) {
      std::cout << "Working on: " << x_a << " of " << len_a << "\r"; 
      std::cout.flush(); 
    }

    boost::scoped_ptr<unordered_map<string, uint>> wm_doc{get_wordmap(*it, 0)};
    std::vector<string> doc_wcounts (corpus_words.size(), "0");

    for (auto i = 0; i != corpus_words.size(); ++i) {
      auto eptr = wm_doc->find(corpus_words[i]); 
      if (eptr != wm_doc->end()) doc_wcounts[i] = boost::lexical_cast<string>(eptr->second);
    }

    of << (int)((*it)[1].as<std::string>() =="t") 
       << "," << boost::algorithm::join(doc_wcounts, ",") << std::endl;
  }

  cout << "Found " << d_tokens << " tokens in the selected corpus." << endl;

  return NULL;
}


int get_gridlock ( pqxx::connection &c ) 
{
  string query = 
    "SELECT newsletter_id, coder, gridlock, snippet"
    " FROM assertion as a"                                             
    " WHERE a.gridlock"                                                 
    " limit 10;"; 

  pqxx::nontransaction n(c); 
  pqxx::result r(n.exec(query)); 

  for (auto it = r.begin(); it != r.end(); ++it) {
    for (auto col = it->begin(); col != it->end(); ++col) {
      cout << col << ",";
    }
    cout << endl;
  }
  
}


int main () 
{
  const string config = 
    "dbname=harbridge_newsletters"
    " hostaddr=52.14.46.49"
    " password=pascalalperjanna"
    " user=nuitrcsuser1";

  pqxx::connection c(config);
  
  if ( !c.is_open()) return 1;

  cout << "Successfully opened " << c.dbname() << endl; 
  
  create_wl(c); 
  
  c.disconnect();

  return 0; 
}
