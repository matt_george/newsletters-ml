
import tensorflow as tf
import parse as p
import numpy as np
import pandas as pd
import csv


## Modifiable parameters ##

# Must point to file containing training and test data.
# Expects a csv of NCOLUMNS where:
#   Col[0] => Labels of observation, either [0,1]
#   Col[1:NCOLUMNS] => features of observation
DATAFILE = 'out-int.txt'

# NCOLUMNS can be set manually or determined automatically from
# the first row of the csv
NCOLUMNS = len(csv.reader(open(DATAFILE, 'rb')).next())
NCOLUMNS= 10 # uncomment to reduce columns for debug purposes

# Use 80-20 split for train and testing data
FRAC = 0.8

# Set to print additional statements
VERBOSE = True

## End parameters ## 


def parse_csv (fname, ncolumns):
    ''' 
    Parse the selected csv [fname] and returns a df of columns 0
    to [ncolumns] with the selected data. Rows are shuffled randomly. 

    returns <dataframe>
    '''
    if VERBOSE: print "Parsing CSV File with", ncolumns, "columns"
    
    dataframe = pd.read_csv(fname, usecols=range(0,ncolumns), header = None)
    return dataframe.sample(frac=1).reset_index(drop=True)  # shuffle in-place


def split_train_test (dataframe, frac):
    '''
    Splits [dataframe] into two objects representing train and test data respectively. 
    [frac]{0.-1.} denotes the percentage of data to include in the train set. The
    remaining data will go to the test test. 

    Returns dtrain <numpy array>, dtest <numpy array>
    '''
    if VERBOSE: print "Splitting dataset into training and test sets"
    
    train = dataframe.sample(frac=frac, random_state=200) #random_state is a seed for rng gen
    test = dataframe.drop(train.index)

    dtrain = lambda:None
    dtest = lambda:None

    # ix (indexing function) usage is <DataFrame>.ix[row slice, column slice]
    dtrain.labels = train.ix[:, 0].values 
    dtrain.features = np.float32(train.ix[:, 1:])
    dtest.labels = test.ix[:, 0].values 
    dtest.features = np.float32(test.ix[:, 1:])

    return dtrain, dtest


def zeror (guess):
    '''
    Always predict guess <uint>
    '''
    return guess


def dist_l1 (x_tr, x_te):
    '''
    Calulates the l1 distance between features vectors [xtrain] and [xtest]
    https://github.com/aymericdamien/TensorFlow-Examples/blob/master/examples/2_BasicModels/nearest_neighbor.py#L42
    '''
    return tf.reduce_sum(tf.abs(tf.add(x_tr, tf.negative(x_te))), reduction_indices=1)


def dist_euclidean (x_tr, x_te):
    '''
    Calulates euclidean distance between features vectors [x_tr] and [x_te]
    http://stackoverflow.com/questions/41131728/problems-with-knn-implemantion-in-tensorflow
    '''
    return tf.negative(tf.sqrt(tf.reduce_sum(tf.square(tf.subtract(x_tr, x_te)),
                                             reduction_indices=1)))


def runsession (name, dtrain, dtest, pred, x_tr, x_te):
    '''
    Run a new sesison using datasets dtrain and dtest, and prediction pred
    '''
    if VERBOSE: print "Starting Session", name
    init = tf.global_variables_initializer()
    
    with tf.Session() as sess:

        correct = wrong = 0.
        sess.run(init)
        
        for i in range(len(dtest.labels)):
            # Calculate nearest neighbor
            nn_idx = sess.run(pred, feed_dict={
                x_tr: dtrain.features,
                x_te: dtest.features[i, :]
            })

            # Get the label associated with nearest neighbor
            prediction = dtrain.labels[nn_idx]
            actual = dtest.labels[i]

            if actual == prediction:
                correct = correct +1
            else:
                wrong = wrong + 1
            acc = correct / (correct + wrong)

            if VERBOSE:
                print i, "Prediction:", prediction, "Actual", actual, "Accuracy:", acc, "[", correct, "/", wrong, "]"
            
        print "Finished", name, "with accuracy of", acc, "[", correct, "/", wrong, "]"


def run_zeror (guess, dtest):
    '''
    Calculate baseline performance by predicting [guess] for each observation in dtest
    '''
    correct = wrong = 0.
    for i in range(len(dtest.labels)):
        prediction = guess
        actual = dtest.labels[i]

        if actual == prediction:
            correct = correct +1
        else:
            wrong = wrong + 1
        acc = correct / (correct + wrong)

        if VERBOSE:
            print i, "Prediction:", prediction, "Actual", actual, "Accuracy:", acc, "[", correct, "/", wrong, "]"
            
    print "Finished Zeror with accuracy of", acc, "[", correct, "/", wrong, "]"

    
def run_distl1 (dtrain, dtest, x_tr, x_te):
    '''
    Runs a session using L1 distance
    '''

    # K = 4
    # # Get minimum distance neighbors
    # values, indices = tf.nn.top_k(dist, k=K, sorted=False)

    # nn = []
    # for i in range (K):
    #     nn.append(tf.argmax(y_tr[indices[i]], 0))

    # nn_tensor = tf.stack(nn)
    # y, idx, count = tf.unique_with_counts(nn_tensor)
    # pred = tf.slice(y, begin=[tf.argmax(count, 0)], size=tf.constant([1], dtype = tf.int64))[0]

    pred = tf.arg_min(dist_l1(x_tr, x_te), 0)
    runsession("L1 Distance", dtrain, dtest, pred, x_tr, x_te)


    
    
def main():
    dtrain, dtest = split_train_test(parse_csv(DATAFILE, NCOLUMNS), FRAC)
    print "Running test with", len(dtrain.labels), "training data elements and", len(dtest.labels), "testing elments"
    
    x_tr = tf.placeholder(tf.float32, [None, NCOLUMNS-1])     # training set
    x_te = tf.placeholder(tf.float32, [NCOLUMNS-1])  # test set

    run_zeror(0, dtest)
    #run_distl1(dtrain, dtest, x_tr, x_te)
    
    
main()



